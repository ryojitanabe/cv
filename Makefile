all:
	# latex document
	# latex document
	# dvips -t letter document
	# ps2pdf document.ps

	platex document
	platex document
	dvipdfmx document
	dvipdfmx -f texfonts.map -p letter document

clean:
	rm -f *.aux *.bbl *.blg *.log document.dvi document.ps document.pdf 
